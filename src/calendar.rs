use chrono::Duration;
use icalendar::{
    Alarm, Calendar as Cal, Component, Event as CalEvent, EventLike, EventStatus, Property,
};
use std::{path::PathBuf, sync::Arc};
use tokio::{fs::File, io::AsyncWriteExt, sync::Mutex};
use url::Url;

use crate::data::FlipEventContent;
use crate::storage::Store;

#[derive(Clone, Debug)]
pub struct Calendar {
    pub url: Option<Url>,
    calendar: Arc<Mutex<Cal>>,
    file_path: PathBuf,
}

impl Calendar {
    pub fn new(file_path: PathBuf, url: Option<Url>) -> Calendar {
        let mut inner = Cal::new();
        inner.name("FLiP gaming events");
        inner.description(
            "Gaming events scheduled by members of the Friendly Linux Players community.",
        );

        Calendar {
            url,
            calendar: Mutex::new(inner).into(),
            file_path,
        }
    }

    pub async fn add_event(&self, event: FlipEventContent, write: bool) {
        let status = if event.cancelled {
            EventStatus::Cancelled
        } else {
            EventStatus::Confirmed
        };

        let organizer = Property::new_pre_alloc(
            "ORGANIZER".to_string(),
            event.host.matrix_to_uri().to_string(),
        )
        .done();

        let ownership_note = if event.ownership_required {
            ""
        } else {
            " Note that ownership of this game is not required to attend the event."
        };

        let permalink = format!("https://friendlylinuxplayers.org/events/{}", event.id_hex(),);

        let description = format!(
            "Join the Friendly Linux Players community in playing {}.{}\n\n\
            See the following web page for more information:\n\
            {}",
            event.game, ownership_note, permalink,
        );

        let alarm = Alarm::display(
            &format!("{} starts in 1 hour.", event.name_or_game()),
            -Duration::hours(1),
        );

        let cal_ev = CalEvent::new()
            .summary(&event.name_or_game())
            .append_property(organizer)
            .description(&description)
            .starts(event.time)
            .ends(event.time + Duration::hours(1))
            .location(&event.meeting_place)
            .url(&permalink)
            .timestamp(event.mx_time)
            .uid(&event.id_hex())
            .sequence(event.generation.into())
            .status(status)
            .alarm(alarm)
            .done();
        self.calendar.lock().await.push(cal_ev);

        if write {
            self.write().await;
        };
    }

    pub async fn populate_events(&self, store: &Store) {
        let option = store.get_events(None).await;
        if let Some(events) = option {
            for e in events {
                self.add_event(e, false).await;
            }
        }
        self.write().await;
    }

    async fn write(&self) {
        let mut file = File::create(self.file_path.clone()).await.unwrap();
        file.write_all_buf(&mut self.calendar.lock().await.to_string().as_bytes())
            .await
            .unwrap();
    }
}
