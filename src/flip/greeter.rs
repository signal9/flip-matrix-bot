use matrix_sdk::room::Room;
use matrix_sdk::ruma::{
    events::{
        room::{
            guest_access::GuestAccess,
            member::{MembershipState, RoomMemberEventContent},
        },
        OriginalSyncStateEvent,
    },
    user_id::OwnedUserId,
};
use rand::{rngs::SmallRng, seq::SliceRandom, RngCore, SeedableRng};
use std::sync::{Mutex, OnceLock};

use super::interface::sender::{self, Message, MessageText};

pub async fn process(ev: OriginalSyncStateEvent<RoomMemberEventContent>, joined_room: Room) {
    let server = joined_room.room_id().server_name().as_str();
    let guest_access = matches!(joined_room.guest_access(), GuestAccess::CanJoin);

    let message = if let Some(RoomMemberEventContent {
        membership: MembershipState::Leave,
        ..
    }) = ev.unsigned.prev_content
    {
        rejoin_message(&ev.sender)
    } else if guest_access && looks_like_guest(&ev.sender, server) {
        guest_message(&ev.sender)
    } else {
        static RNG: OnceLock<Mutex<SmallRng>> = OnceLock::new();
        let rng = RNG.get_or_init(|| SmallRng::from_entropy().into());
        new_message(&ev.sender, &mut *rng.lock().unwrap())
    };

    sender::send(message, joined_room, None, None).await;
}

fn rejoin_message(joiner: &OwnedUserId) -> Message {
    Message::Unthreaded(format!("Welcome back, [{}]({})!", joiner, joiner.matrix_to_uri()).into())
}

fn looks_like_guest(joiner: &OwnedUserId, server: &str) -> bool {
    joiner.localpart().parse::<u64>().is_ok() && joiner.server_name().host() == server
}

fn guest_message(joiner: &OwnedUserId) -> Message {
    let guest_text = MessageText::new(format!(
        "[{}]({}), welcome to the [Friendly Linux Players](https://friendlylinuxplayers.org) \
        community! It looks like you've joined with a guest account, which only has temporary \
        access. To stick around longer, create a new account using the account menu!",
        joiner,
        joiner.matrix_to_uri(),
    ));

    Message::Unthreaded(guest_text)
}

fn new_message(joiner: &OwnedUserId, rng: &mut impl RngCore) -> Message {
    let greetings = [
        "Good to see you",
        "Greetings",
        "Hello",
        "Hey",
        "Hey there",
        "Hi",
        "Salutations",
    ];
    let prompts = [
        "Name a game that you have a lot of nostalgia for.",
        "Name a game that you wish you could get others to play with you.",
        "What brought you to our community?",
        "What does your ideal gaming session look like?",
        "What game do you have the most playtime in?",
        "What game genres do you typically play?",
        "What games have you been playing recently?",
        "What games have you purchased recently?",
        "What is the first video game you've ever played?",
        "What is the last game you completed?",
        "What is the top game on your wishlist?",
        "What is your favorite co-op game?",
        "What is your favorite competitive/adversarial game?",
        "What is your favorite game of all time?",
        "What is your favorite multiplayer game?",
        "What is your favorite single-player game?",
        "What is your favorite style of video game storytelling?",
        "What is your favorite video game soundtrack?",
        "What is your go-to multiplayer game?",
        "What is your most anticipated upcoming game?",
        "What kind of hardware are you playing games with?",
        "What sort of game do you wish there was more of?",
        "Tell us about a fond memory you have of a gaming experience.",
        "Tell us about a game that you think is underappreciated.",
        "Tell us about a mechanic or feature in a game that you think is really cool.",
    ];

    let message_text = MessageText::new(format!(
        "{}, [{}]({})! Welcome to the main Matrix chat room for the [Friendly Linux \
        Players](https://friendlylinuxplayers.org) community. For our other rooms, check out the \
        Matrix space: [#community:flip.earth](https://matrix.to/#/#community:flip.earth).\n\n\
        Let's get to know each other! {}",
        greetings.choose(rng).unwrap(),
        joiner,
        joiner.matrix_to_uri(),
        prompts.choose(rng).unwrap(),
    ));

    Message::Unthreaded(message_text)
}

#[cfg(test)]
mod tests {
    use super::*;
    use matrix_sdk::ruma::user_id;
    use rand::rngs::mock::StepRng;

    #[test]
    fn composing_rejoin_greetings() {
        let msg = rejoin_message(&user_id!("@test:server.domain").to_owned());
        let Message::Unthreaded(msg) = msg else { panic!("Invalid message type") };
        assert_eq!(
            msg.text,
            "Welcome back, [@test:server.domain](https://matrix.to/#/@test:server.domain)!"
        );
    }

    #[test]
    fn composing_guest_greetings() {
        let msg = guest_message(&user_id!("@test:server.domain").to_owned());
        let Message::Unthreaded(msg) = msg else { panic!("Invalid message type") };
        assert_eq!(
            msg.text,
            "[@test:server.domain](https://matrix.to/#/@test:server.domain), welcome to the \
            [Friendly Linux Players](https://friendlylinuxplayers.org) community! It looks like \
            you've joined with a guest account, which only has temporary access. To stick around \
            longer, create a new account using the account menu!"
        );
    }

    #[test]
    fn composing_new_greetings() {
        let mut rng = StepRng::new(0, 0x1f1f1f1f1f1f1f1f);

        let msg = new_message(&user_id!("@test:server.domain").to_owned(), &mut rng);
        let Message::Unthreaded(msg) = msg else { panic!("Invalid message type") };
        assert_eq!(
            msg.text,
            "Good to see you, \
            [@test:server.domain](https://matrix.to/#/@test:server.domain)! \
            Welcome to the main Matrix chat room for \
            the [Friendly Linux Players](https://friendlylinuxplayers.org) \
            community. For our other rooms, check out the Matrix space: \
            [#community:flip.earth](https://matrix.to/#/#community:flip.earth).\n\n\
            Let's get to know each other! \
            What does your ideal gaming session look like?"
        );

        let msg = new_message(&user_id!("@test:server.domain").to_owned(), &mut rng);
        let Message::Unthreaded(msg) = msg else { panic!("Invalid message type") };
        assert_eq!(
            msg.text,
            "Greetings, \
            [@test:server.domain](https://matrix.to/#/@test:server.domain)! \
            Welcome to the main Matrix chat room for \
            the [Friendly Linux Players](https://friendlylinuxplayers.org) \
            community. For our other rooms, check out the Matrix space: \
            [#community:flip.earth](https://matrix.to/#/#community:flip.earth).\n\n\
            Let's get to know each other! \
            What is the last game you completed?"
        );
    }

    #[test]
    fn user_id_looks_like_guest() {
        let user1 = user_id!("@test:server.domain").to_owned();
        let user2 = user_id!("@test:flip.earth").to_owned();
        let user3 = user_id!("@12345:server.domain").to_owned();
        let guest = user_id!("@12345:flip.earth").to_owned();

        assert_eq!(looks_like_guest(&user1, "flip.earth"), false);
        assert_eq!(looks_like_guest(&user2, "flip.earth"), false);
        assert_eq!(looks_like_guest(&user3, "flip.earth"), false);
        assert_eq!(looks_like_guest(&guest, "flip.earth"), true);
    }
}
